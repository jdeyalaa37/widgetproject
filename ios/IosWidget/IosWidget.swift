//
//  IosWidget.swift
//  IosWidget
//
//  Created by Mohamed Ali BELHADJ on 2022-08-08.
//

import WidgetKit
import SwiftUI
import Intents

struct widgetData :Decodable {
  var text: String
  var img: String
  var isUrl: Bool
}

struct Provider: IntentTimelineProvider {
  func placeholder(in context: Context) -> SimpleEntry {
    SimpleEntry(date: Date(), text:"Placeholder ",configuration: ConfigurationIntent(),img:"",isUrl:false)
  }
  
  func getSnapshot(for configuration: ConfigurationIntent, in context: Context, completion: @escaping (SimpleEntry) -> ()) {
    let entry = SimpleEntry(date: Date(), text:"text goes here",configuration: configuration,img:"Image goes here",isUrl:false)
    completion(entry)
  }
  
  func getTimeline(for configuration: ConfigurationIntent, in context: Context, completion: @escaping (Timeline<SimpleEntry>) -> Void) {
    
    let userDefaults = UserDefaults.init(suiteName: "group.ProjectName.app")
    if userDefaults != nil {
      let entryDate = Date()
      if let savedData = userDefaults?.string(forKey: "myAppData"){
        let decoder = JSONDecoder()
        let data = savedData.data(using: .utf8)
        if let parsedData = try? decoder.decode(widgetData.self, from: data!) {
          let nextRefresh = Calendar.current.date(byAdding: .minute, value: 5, to: entryDate)!
          let entry = SimpleEntry(date: nextRefresh, text: parsedData.text, configuration: configuration, img: parsedData.img, isUrl: parsedData.isUrl)
          let timeline = Timeline(entries: [entry], policy: .atEnd)
          completion(timeline)
          
        } else {
          print("Could not parse data")
        }
      } else {
        let nextRefresh = Calendar.current.date(byAdding: .minute, value: 5, to: entryDate)!
        let entry = SimpleEntry(date: nextRefresh, text: "No data set", configuration: configuration, img: "No Image", isUrl: false)
        let timeline = Timeline(entries: [entry], policy: .never)
        completion(timeline)
      }
      
    }
  }
  
}

struct SimpleEntry: TimelineEntry {
  var date: Date
  let text: String
  let configuration: ConfigurationIntent
  let img: String
  let isUrl: Bool
}
struct ImageOverlay: View {
  var entry: Provider.Entry
  var body: some View {
    ZStack {
      Text(entry.text)
        .font(.callout)
        .padding(6)
        .foregroundColor(.white)
    }.background(Color.black)
      .opacity(0.8)
      .cornerRadius(10.0)
      .padding(6)
  }
}

struct IosWidgetEntryView: View {
  var entry: Provider.Entry
  var body: some View {
    if entry.isUrl == true {
      let fileName = entry.img
      HStack {
        VStack(alignment: .leading) {
          GeometryReader { geo in
            Image(uiImage: UIImage(data: Data(base64Encoded: fileName) ?? Data()) ?? UIImage())
              .resizable()
              .cornerRadius(16)
              .aspectRatio(contentMode: .fit)
              .frame(width: geo.size.width * 0.9)
              .frame(width: geo.size.width, height: geo.size.height)
            
          }
          Spacer()
          Text(entry.text)
            .foregroundColor(Color(red: 115 / 255, green: 85 / 255, blue: 7 / 255))
            .font(Font.system(size: 16, weight: .bold, design: .rounded))
            .padding([.top, .bottom], 15)
            .frame(maxWidth: .infinity)
            .background(Color(red: 248 / 255, green: 220 / 255, blue: 150 / 255))
            .cornerRadius(16)
        }
      }
      .frame(maxWidth: .infinity, maxHeight: .infinity)
      .padding(15)
      .background(Color(red: 251 / 255, green: 236 / 255, blue: 197 / 255))
    }
    else if entry.isUrl == false {
      let fileName = entry.img
      
      HStack {
        VStack(alignment: .center) {
          GeometryReader { geo in
            Image(uiImage: UIImage(data: Data(base64Encoded: fileName) ?? Data()) ?? UIImage())
              .resizable()
              .cornerRadius(10)
              .aspectRatio(contentMode: .fit)
              .frame(width: geo.size.width * 0.9)
              .frame(width: geo.size.width, height: geo.size.height)
            
            
          }
          Text(entry.text)
            .foregroundColor(Color(red: 115 / 255, green: 85 / 255, blue: 7 / 255))
            .font(Font.system(size: 16, weight: .bold, design: .rounded))
            .padding([.top, .bottom], 15)
            .frame(maxWidth: .infinity)
            .background(Color(red: 248 / 255, green: 220 / 255, blue: 150 / 255))
            .cornerRadius(16)
            .opacity(0.3)
        }
        
      }
      .frame(maxWidth: .infinity, maxHeight: .infinity)
      .padding(15)
      .background(Color(red: 251 / 255, green: 236 / 255, blue: 197 / 255).opacity(0.5))
    }
  }
}

@main
struct IosWidget: Widget {
  let kind: String = "IosWidget"
  var body: some WidgetConfiguration {
    IntentConfiguration(kind: kind, intent: ConfigurationIntent.self, provider: Provider()) { entry in
      IosWidgetEntryView(entry: entry)
    }
    .configurationDisplayName("Supernova")
  }
}

struct IosWidget_Previews: PreviewProvider {
  static var previews: some View {
    IosWidgetEntryView(entry: SimpleEntry(date: Date(), text:"",configuration: ConfigurationIntent(),img:"",isUrl:false))
      .previewContext(WidgetPreviewContext(family: .systemLarge))
  }
}

