import React, {useEffect, useState} from 'react';
import {Button, Image, Pressable, Text, TextInput, View} from 'react-native';
import {NativeModules} from 'react-native';
import SharedGroupPreferences from 'react-native-shared-group-preferences';
const appGroupIdentifier = 'group.ProjectName.app';
const {SharedStorage} = NativeModules;
import ImagePicker from 'react-native-image-crop-picker';
import RNWidgetCenter from 'react-native-widget-center';
export const URL_REGEX = new RegExp(
  /^(?:https?|ftp):\/\/(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4])|(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*\.[a-z\u00a1-\uffff]{2,}\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/i,
);

const App = () => {
  const [image, setImage] = useState(
    'https://i.picsum.photos/id/893/200/300.jpg?hmac=7jsxm2l6ji-5CBnfrJO7IqDUekLtP4PvA7taLcRW2NI',
  );
  const [viewImage, setViewImage] = useState(
    'https://i.picsum.photos/id/893/200/300.jpg?hmac=7jsxm2l6ji-5CBnfrJO7IqDUekLtP4PvA7taLcRW2NI',
  );
  const takePhoto = () => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      includeBase64: true
    }).then(image => {
      console.log(image.path);
      setImage(image.data);
      setViewImage(image.path);
    });
  };
  const ChoosePhoto = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      includeBase64: true
    }).then(image => {
      console.log(image);
      setImage(image.data);
      setViewImage(image.path);

      RNWidgetCenter.reloadAllTimelines();
    });
  };
  const [text, setText] = useState('Supernova');
  useEffect(() => {
    SharedStorage?.set(
      JSON.stringify({
        text: text,
        img: viewImage,
        isUrl: URL_REGEX.test(viewImage),
      }),
    );
  }, [viewImage, text]);
  const widgetData = {
    text: text,
    img: image,
    isUrl: URL_REGEX.test(image),
  };
  const saveUserDataToSharedStorage = () => {
    return new Promise(async (resolve, reject) => {
      if (widgetData !== null) {
        try {
          const appGroupIdentifier = 'group.ProjectName.app';
          await SharedGroupPreferences?.setItem(
            'myAppData',
            widgetData,
            appGroupIdentifier,
          );
          RNWidgetCenter.reloadAllTimelines();
          resolve({widgetData, appGroupIdentifier});
        } catch (errorCode) {
          reject(errorCode);
        }
      }
    });
  };

  useEffect(() => {
    saveUserDataToSharedStorage().then(r => console.log(r));
  }, [image, text]);

  return (
    <View>
      <Image source={{uri: viewImage}} style={{width: 100, height: 100}} />
      <Text>Please SetText</Text>
      <TextInput
        style={{borderColor: '#000', width: 400, height: 30}}
        value={text}
        onEndEditing={saveUserDataToSharedStorage}
        onChangeText={val => {
          setText(val);
        }}
      />
      <Pressable
        style={{width: 100, height: 30, backgroundColor: 'blue'}}
        onPress={ChoosePhoto}>
        <Text style={{color: 'red'}}>Add</Text>
      </Pressable>
    </View>
  );
};

export default App;
